# **批量文件改名（filerename）**

#### 一、介绍
​	简单易上手，不失灵活的规则定义，满足你个性化的改名需要。绿色软件，速度快。

#### 二、下载地址
本站下载：[批量文件改名（filerename）](https://gitee.com/xibinhui/filerename/tree/master/bin)

百度网盘下载：[批量文件改名（filerename）](https://pan.baidu.com/s/1QCsV62aI_hOenw4-f0Ocqw?pwd=xibh#list/path=%2Fxibinhui%2Ffilerename)

#### 三、安装教程
1. 将下载的文件解压到D:\xibinhui，D:\Program\xibinhui或者其他你规划的目录中。

   注意事项：

   ​	1）请不要解压到桌面。
   ​	2）请不要解压到系统所在的盘符中。
   ​	3）请不要在压缩文件中运行本程序。

2. 我的安装路径为D:\Program\xibinhui\filerename

3. 安装完成。双击filerename.exe即可打开本程序。

#### 四、使用说明
1. ​	**主界面**
    ![](https://gitee.com/xibinhui/filerename/raw/master/doc/01.jpg)
    工具分为六步：
    ​	1）选择目录。你要改名的文件在该目录下。
    ​	2）设置过滤条件、查找文件。目录中有你不想改名的文件，可设置过滤条件。
    ​	3）添加改名规则。现在有添加、删除、替换文字三大类规则。
    ​	4）预览改名。点击按钮执行后，在”新文件名“列中显示你设置规则的结果。
    ​	5）执行改名。预览改名结果确认无误后，点击按钮执行文件改名动作。
    ​	6）结束。
    **注意：”执行改名“后，文件名无法恢复原来的模样，请谨慎操作。**

  

2. ​	**改名规则**
    ![](https://gitee.com/xibinhui/filerename/raw/master/doc/02.jpg)
    添加文字：在文件名添加设置的文字。
    添加数字：可在文件名中添加序号。
    添加时间：可在文件名中添加年月日等。
    删除文字：删除文件名不想要的文字。
    替换文字：替换文件名的文字。

  

3. ​	**添加文字规则**
    ![](https://gitee.com/xibinhui/filerename/raw/master/doc/03.jpg)

  

4. ​	**添加数字规则**
    ![](https://gitee.com/xibinhui/filerename/raw/master/doc/04.jpg)
    
    
    
5. ​	**添加时间规则**
    ![](https://gitee.com/xibinhui/filerename/raw/master/doc/05.jpg) 



6. ​	**删除文字规则**
    ![](https://gitee.com/xibinhui/filerename/raw/master/doc/06.jpg) 

  

7. ​	**替换文字规则**
    ![](https://gitee.com/xibinhui/filerename/raw/master/doc/07.jpg) 
    
    
    

#### 五、版本历史
v1.3（2023-12-8）
    1）修改BUG

v1.2（2021-4-8）
    1）删除、替换规则中增加对扩展名的处理选项。
    2）增强删除规则。
	
v1.1（2020-11-29）
	1）更新发布了，增加目录拖放。

v1.0（2020-11-02）
	1）发布了。

   

#### 六、支持

1. 软件问题：在线论坛 - [Issues](https://gitee.com/xibinhui/filerename/issues)
2. 功能需求：邮箱 - leashi@163.com
3. 支付故事：QQ号 - 79534262  QQ群 - 314714697

